import * as t from 'io-ts';
import { reporter } from 'io-ts-reporters';
import { nonEmptyArray } from 'io-ts-types/lib/NonEmptyArray';
import { NonEmptyString } from 'io-ts-types/lib/NonEmptyString';
import { UUID } from 'io-ts-types/lib/UUID'

const buildObjectCodec = <P extends t.Props>(props: P) => t.readonly(t.exact(t.type(props)));

const structureCodec = buildObjectCodec({
    nea: nonEmptyArray(t.number),
    nes: NonEmptyString,
    optionalNumber: t.union([t.number, t.undefined]),
    uuid: UUID,
});

type Structure = t.TypeOf<typeof structureCodec>;

{
    const correctStructure = {
        nea: [1, 2, 3],
        nes: '!',
        uuid: '00000000-0000-0000-0000-000000000000'
    }
    
    const either = structureCodec.decode(correctStructure);
    
    if ('right' in either) {
        const structure: Structure = either.right;
        console.log('RESULT:', structure);
    } else {
        console.log('ERROR:', either.left);
    }
}
{
    const incorrectStructure = {
        nea: [],
        nes: '',
    }
    
    const either = structureCodec.decode(incorrectStructure);
    
    if ('right' in either) {
        const structure: Structure = either.right;
        console.log('RESULT:', structure);
    } else {
        console.log('ERROR:', reporter(either));
    }
}



import * as t from 'io-ts';
import { IntFromString } from 'io-ts-types/lib/IntFromString';
import { NumberFromString } from 'io-ts-types/lib/NumberFromString';
import { DateFromUnixTime } from 'io-ts-types/lib/DateFromUnixTime';
import { DateFromISOString } from 'io-ts-types/lib/DateFromISOString';
import { BooleanFromString } from 'io-ts-types/lib/BooleanFromString';
import { reporter } from 'io-ts-reporters';

const buildObjectCodec = <P extends t.Props>(props: P) => t.readonly(t.exact(t.type(props)));

const structureCodec = buildObjectCodec({
    intFromString: IntFromString, // branded, "opaque" type that "extends" number
    numberFromString: NumberFromString,
    dateFromUnixTime: DateFromUnixTime,
    dateFromISOString: DateFromISOString,
    booleanFromString: BooleanFromString,
});

type Structure = t.TypeOf<typeof structureCodec>;

const now = new Date();

const input = {
    intFromString: "10",
    numberFromString: "1.234",
    dateFromUnixTime: (now.getTime() / 1000) | 0, // why do we need to force it into an "integer" value?
    dateFromISOString: now.toISOString(),
    booleanFromString: "true",
}

const structureEither = structureCodec.decode(input);

if ('right' in structureEither) {
    const structure: Structure = structureEither.right;
    console.log('RESULT:', structure);
} else {
    console.log(reporter(structureEither));
}


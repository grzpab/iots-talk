import * as t from 'io-ts';
import { reporter } from 'io-ts-reporters';

const buildObjectCodec = <P extends t.Props>(props: P) => t.readonly(t.exact(t.type(props)));

// imagine a TCP/IP architecture (e.g WebSockets) in which a client can log in and log out using JSON commands.
const logInCommandCodec = buildObjectCodec({
    type: t.literal('LOG_IN'),
    username: t.string,
    password: t.string,
});

const logOutCommandCodec = buildObjectCodec({
    type: t.literal('LOG_OUT'),
    token: t.string,
});

// we accept commands of all types using a single deserializer
const commandCodec = t.union([
    logInCommandCodec,
    logOutCommandCodec,
]);

// logging in
{
    const data = `{"type":"LOG_IN","username":"a","password":"b"}`;
    const json = JSON.parse(data);
    
    const commandEither = commandCodec.decode(json);
    
    if ('right' in commandEither) {
        const command = commandEither.right;
        console.log('LOG_IN structure', command);
    } else {
        console.log(reporter(commandEither));
    }
}

// logging out
{
    const data = `{"type":"LOG_OUT","token":"1234"}`;
    const json = JSON.parse(data);
    
    const commandEither = commandCodec.decode(json);
    
    if ('right' in commandEither) {
        const command = commandEither.right;
        console.log('LOG_OUT structure', command);
    } else {
        console.log(reporter(commandEither));
    }
}

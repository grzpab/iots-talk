import * as t from 'io-ts'; // `t` by convention
import { reporter } from 'io-ts-reporters';

// helper for serializing generic objects
const buildObjectCodec = <P extends t.Props>(props: P) => t.readonly(t.exact(t.type(props)));

const bookCodec = buildObjectCodec({
    name: t.string,
    author: t.string,
    releaseYear: t.number, // it is not an integer!
});

type Book = t.TypeOf<typeof bookCodec>; // type extraction

const data = `{
    "name":"The Fall of Gondolin",
    "author":"J. R. R. Tolkien",
    "releaseYear":2018,
    "additionalProperty":true
}`;

const unknownBook = JSON.parse(data); // of type any

// decode accepts an `unknown` by default and return an Either<Errors, Book>
const bookEither = bookCodec.decode(unknownBook);

if ('right' in bookEither) { // or isRight(bookEither)
    const book: Book = bookEither.right;
    console.log(book);
} else {
    console.log(reporter(bookEither));
}

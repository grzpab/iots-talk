# Basic usage

The library characteristics:
* uses functional programming patterns and structures leveraged in `fp-ts`:
    - stateless functions,
    - immutability,
    - composition of functions and objects,
    - monads (`Either<L, R> = Left<L> | Right<R>` structure),
* validation (decoding) is done using composable `codecs` (serializer objects),
* every validation result is of type `Either<Errors, T>`,
* codecs can force-omit additional properties,
* codecs can be used to define types from themselves by the `TypeOf` type transformer.

[Code example](../src/03.ts)

[->](04.md)


